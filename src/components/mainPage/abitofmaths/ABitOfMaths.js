import React from 'react';
import './abitofmaths.css'

const ABitOfMaths = () => {
    return (
        <div className="abitofmaths_main ">
            <div className={"abitofmaths_main-fl"}>
                <div className={"abitofmaths_main-fl-text"}>
                    Немного <br/>
                    математики
                </div>
                <div className={"abitofmaths_main-fl-circle"}>
                    <p className={"title"}>73%</p>
                    <p>всех обучившихся <br/> нашли работу</p>
                </div>
            </div>
            <div className={"abitofmaths_main-sl"}>
                <div className={"abitofmaths_main-sl-circle"}>
                        <p className={"title"}>3 550+</p>
                        <p>студентов прошли у нас</p>
                        <p>обучение за всё время</p>
                </div>
            </div>
            <div className={"abitofmaths_main-tl"}>
                <div className={"abitofmaths_main-tl-circle"}>
                    <p className={"title"}>400+</p>
                    <p>компаний-партнеров, <br />
                        которые готовы брать<br /> выпускников
                        на <br/>работу</p>
                </div>
            </div>
        </div>
    );
};

export default ABitOfMaths;