import React, {useState} from 'react';
import "./coollearning.css"
import deco1 from "../../../assets/mainPage/coollearning.svg"
import slider1 from "../../../assets/mainPage/lc_photo1.png"
import slider2 from "../../../assets/mainPage/slider2.png"
import slider3 from "../../../assets/mainPage/slider3.png"
import slider4 from "../../../assets/mainPage/slider4.png"
import slider5 from "../../../assets/mainPage/slider5.png"
import slider6 from "../../../assets/mainPage/slider6.png"
import slider7 from "../../../assets/mainPage/slider7.png"
import slider8 from "../../../assets/mainPage/slider8.png"
import slider9 from "../../../assets/mainPage/slider9.png"
import {ProGallery} from 'pro-gallery';
import 'pro-gallery/dist/statics/main.css';
import {observer} from "mobx-react-lite";
import ScrollHorizontal from 'react-scroll-horizontal';

const CoolLerning = observer(() => {

    const images = [
        {
            img: slider1,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        },
        {
            img: slider2,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/В данном направлении. Освоив SEO - вы становитесь крутым маркетологом."
        },
        {
            img: slider3,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        },
        {
            img: slider4,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        },
        {
            img: slider5,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        }, {
            img: slider6,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        }, {
            img: slider7,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        }, {
            img: slider8,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        }, {
            img: slider9,
            firstname: "Имя",
            lastname: "Фамилия",
            description: "/На рынке Узбекистана для многих " +
                "SEO-продвижение является больной темой, так " +
                "как в стране кадровый кризис в данном направлении. " +
                "Освоив SEO - вы становитесь крутым маркетологом."
        },


    ]

    const [currentImage, setCurrentImage] = useState(images[0])


    const changeImage = (image) => {
        setCurrentImage(image)
    }

    return (<>
        <div className={"coollerning_main"}>
            <div className={"coollerning_main-title"}>
                <p>
                    Учиться в Tehnikum <br/> интересно и точно полезно
                </p>
                <img src={deco1} alt={""}></img>
            </div>
            <div className={"coollerning_main-photoandinfo"}>
                <div className={"photo"}>
                    <img src={currentImage.img} alt={""}></img>
                    <p className={"title"}>{currentImage.firstname} <br/> {currentImage.lastname}</p>
                    <p className={"description"}>{currentImage.description}</p>
                </div>
                <div className={"info"}>
                    <div className={"text"}>
                        <p>Все наши преподаватели --<br/>действующие специалисты.</p>
                        <br/>
                        <p>Но быть профессионалом<br/>
                            не ознаечает быть<br/> душнилой,
                            поэтому с ними<br/> ещё и весело. </p>
                    </div>
                    <div className={'button'}>Хочу в команду</div>
                </div>
            </div>
            <div className={"coollerning_main_scrollbar"}>
                <ScrollHorizontal>
                    {images.map((image, idx) => (
                        <img key={idx} onClick={() => changeImage(image)} src={image.img} alt={""}></img>))}
                </ScrollHorizontal>
            </div>
        </div>
        <div className={"coollerning_main-mobile"}>
            <div className={"coollerning_main-title"}>
                <p>
                    Учиться в <br/> Tehnikum <br/> интересно и <br/> точно полезно
                </p>
                <img src={deco1} alt={""}></img>
            </div>
            <div className={"coollerning_main-mobile-scroller"}>
                <ScrollHorizontal>
                    {images.map((image, idx) => (
                        <div key={idx} className={"coollerning_main-mobile-scroller-item"}>
                            <img src={image.img} alt={""}></img>
                            <div className={"coollerning_main-mobile-scroller-title"}>{image.firstname}
                                <br/> {image.lastname}
                            </div>
                            <div className={"coollerning_main-mobile-scroller-description"}>{image.description}</div>
                        </div>
                    ))}
                </ScrollHorizontal>
            </div>
        </div>
    </>);
});

export default CoolLerning;