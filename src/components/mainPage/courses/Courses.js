import React from 'react';
import './courses.css'
import arrowRight from '../../../assets/courses/arrowRight.png'

const Courses = () => {
    return (<div className={'courses'}>
        <div className={'courses-item courses-marketing'}>
            <p className={'courses-marketing-default__text courses-item-text'}>Маркетинг</p>
            <p className={'courses-marketing-hover__text courses-item-text'}>Маркетинг / 8 курсов / SMM, таргет,
                продукт</p>
            <p className={'courses-item-small__text'}>8 курсов / SMM, таргет, продукт</p>
            <img src={arrowRight} alt={''}></img>
        </div>
        <div className={'courses-item courses-programming'}>
            <p className={'courses-programming-default__text courses-item-text'}>Программирование</p>
            <p className={'courses-programming-hover__text courses-item-text'}>Программирование / 8 курсов / frontend,
                backend,
                python</p>
            <p className={'courses-item-small__text'}>3 курса / Java, Python</p>
            <img src={arrowRight} alt={''}></img>
        </div>
        <div className={'courses-item courses-design'}>
            <p className={'courses-design-default__text courses-item-text'}>Дизайн</p>
            <p className={'courses-design-hover__text courses-item-text'}>Дизайн / 8 курсов / digital, figma</p>
            <p className={'courses-item-small__text'}>1 курс / коммуникации, веб</p>
            <img src={arrowRight} alt={''}></img>
        </div>
    </div>);
};

export default Courses;