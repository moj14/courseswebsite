import React from 'react';
import "./graduate.css"
import girlWithBook from "../../../assets/mainPage/girlWithBook.png"
import star from "../../../assets/mainPage/star.svg"

const Graduate = () => {
    return (
        <div className={'graduate'}>
            <div className={"graduate-photo"}>
                <img src={girlWithBook} alt={"Тут девочка с книгой"}></img>
            </div>
            <div className={"graduate-info"}>
                <div className={"graduate-info-text"}>
                    Наши дипломы <br/> аккредитированы<br/>
                    Маркетинговой<br/> Ассоциацией<br/> Узбекистана.
                </div>
                <div className={"graduate-info-star"}>
                    <img src={star} alt={"Тут звезда"}></img>
                </div>
            </div>
        </div>
    );
};

export default Graduate;