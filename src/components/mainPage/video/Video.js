import React, {useRef, useState} from 'react';
import './video.css'
import video from '../../../assets/video/video.mp4'
import poster from '../../../assets/video/poster.png'

const Video = () => {
    const [isPlaying, setPlaying] = useState(false)
    const vidRef = useRef(null);
    const playVideo = () => {
        vidRef.current.play();
        setPlaying(true)
    }
    const stopVideo = () => {
        vidRef.current.pause();
        setPlaying(false)
    }

    return (
        <div className={'video'}>
            <video poster={poster} ref={vidRef}>
                <source src={video} type="video/mp4"/>
            </video>
            {
                isPlaying ? (
                    <div onClick={() => {
                        stopVideo()
                    }} className={'video-controller video-controller-stop'}>
                        <p>Пауза</p>
                    </div>
                ) : (
                    <div onClick={() => {
                        playVideo()

                    }} className={'video-controller video-controller-start'}>
                        <p>Смотреть</p>
                    </div>
                )
            }


        </div>
    );
};

export default Video;