import React from 'react';
import "./way.css"

const Way = () => {
    return (
        <div className={"way"}>
            <div className={"way-element"}>Заявка<br />
                на обучение <br />
                в Tehnikum </div>
            <div className={"way-element"}>Лекции с<br />
                креативным
                <br /> подходом</div>
            <div className={"way-element"}>Практика и<br /> наставничес
                <br /> тво</div>
            <div className={"way-element-black"}>Диплом</div>
        </div>
    );
};

export default Way;