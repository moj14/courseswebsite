import React from 'react';
import './welcome.css'
import arrowRight from '../../../assets/mainPage/arrowRight.svg'
import play from '../../../assets/mainPage/play.svg'
import sqr from '../../../assets/mainPage/sqr.png'

const Welcome = () => {
    return (
        <>
            <div className={'welcome section__margin'}>
                <div className={'welcome-text-container-small'}>
                    <p>Оффлайн- <br/> &nbsp; &nbsp; &nbsp; университет <br/> IT профессий</p>
                </div>
                <div className={'welcome-text-main'}><p>Школа актуальных профессий с нуля до трудоустройства</p></div>
                <div className={'welcome-bottom-content-row'}>
                    <div className={'welcome-bottom-buttons'}>
                        <div className={'welcome-button-course'}>
                            <p>Подобрать курс</p>
                            <img className={'welcome-arrowRight-img'} src={arrowRight} alt={''}></img>
                        </div>
                        <div className={'welcome-button-video'}>
                            <img className={'welcome-play-img'} src={play} alt={''}></img>
                            <p>Видео о школе</p>
                        </div>
                    </div>
                    <div className={'welcome-bottom-info'}>
                        <p className="info-people">3 550+</p>
                        <p className={'info-content'}>человек уже освоили новую профессию и <br/> устроились на
                            высокоплачиваемую
                            работу <br/> благодаря нашим
                            курсам</p>
                    </div>
                </div>
            </div>
            <div className={'welcome-under-text section__margin'}>
                <div className={'welcome-under-text-about__us'}><p>
                    Мы не инфоцыгане — и не огромная онлайн-школа <img src={sqr} alt={''}></img> <br/>
                    Мы не сделаем вас миллиардерами и не научим быть <br/> счастливыми —. Все, что мы умеем, — это
                    хорошо
                    обучать.
                </p></div>
                <div className={'welcome-under-text-dont__worry'}><p>
                    Не бойтесь пробовать и набивать шишки. <br/> Tehnikum <span className={'welcome-round-img'}>поддержит вас на всех этапах</span><br/> обучения.
                </p></div>
            </div>
            <div className={'welcome-buttons-under'}>
                <div className={'welcome-button-course'}>
                    <p>Подобрать курс</p>
                    <img className={'welcome-arrowRight-img'} src={arrowRight} alt={''}></img>
                </div>
                <div className={'welcome-button-video'}>
                    <img className={'welcome-play-img'} src={play} alt={''}></img>
                    <p>Видео о школе</p>
                </div>
            </div>

        </>

    );
};

export default Welcome;