import React, {useState} from 'react';
import logoBlack from "../../../assets/others/FrameBlack.png"
import logoWhite from "../../../assets/others/FrameWhite.png"
import menu from "../../../assets/others/menu.png"
import cross from "../../../assets/others/cross.png"
import './navBar.css'
import arrowDown from "../../../assets/others/arrowDown.png";
import roundStar from "../../../assets/others/round_star.png";

const Menu = () => {
    return (
        <div className={'navbar-menu-small scale-up-center'}>
            <div className={'navbar-menu-small-container'}>
                <div className={'navbar-menu-small-container__content'}>
                    <div>
                        <div className={'d-flex navbar-menu-small-buttons'}>
                            <p><a>Главная</a></p>
                            <p><a>Курсы</a></p>
                            <p><a>Карьера</a></p>
                            <p><a>Контакты</a></p>
                        </div>
                        <div className={'navbar-menu-small-container__difficulty'}>
                            <p className={'muted'}>Курсы по сложности</p>
                            <div className={'navbar-menu-small-container__difficulty-content'}>
                                <div className={'d-flex difficulty-content-item'}><img src={roundStar} alt={''}></img>
                                    <p>Для начинающих</p>
                                </div>
                                <div className={'d-flex difficulty-content-item'}><img src={roundStar} alt={''}></img>
                                    <p>Для специалистов</p>
                                </div>
                                <div className={'d-flex difficulty-content-item'}><img src={roundStar} alt={''}></img>
                                    <p>Мастер-классы</p>
                                </div>
                            </div>
                        </div>
                        <div className={'navbar-menu-small-container__difficulty'}>
                            <p className={'muted'}>Курсы по направлениям</p>
                            <div className={'navbar-menu-small-container__difficulty-content'}>
                                <div className={'d-flex'}><p>Маркетинг</p>
                                </div>
                                <div className={'d-flex'}><p>Программирование</p>
                                </div>
                                <div className={'d-flex'}><p>Дизайн</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}


const NavBar = () => {
    const [isOpen, setOpen] = useState(false)
    return (
        <>
            <div className={"navbar"}>
                <div className={'navbar-container'}>
                    <div className="navbar-start">
                        <div className="navbar-start-img">
                            <img src={logoBlack} alt={'logo'}></img>
                        </div>

                    </div>
                    <div className="navbar-center">
                        <p><a>Ул. Афросиаб 2</a></p>
                        <p><a>+998 97 133-35-55</a></p>
                    </div>
                    <div className="navbar-end">
                        <button type={'button'}>
                            <div className={'arrow-button'}>курсы
                                <img src={arrowDown} alt={''}></img>
                            </div>
                        </button>
                        <button type={'button'}>карьера</button>
                        <button type={'button'}>контакты</button>
                    </div>
                </div>
                <div className={`navbar-container-small ${
                    isOpen ? 'bg-white' : 'bg-black'
                }`}>
                    <div className="navbar-start-img">
                        <img src={isOpen ? logoBlack : logoWhite} alt={'logo'}></img>
                    </div>
                    <div className={'navbar-end'}>
                        <img src={isOpen ? cross : menu} onClick={() => isOpen ? setOpen(false) : setOpen(true)}
                             alt={'menu'}></img>
                    </div>
                </div>
            </div>
            {
                isOpen ? (
                    <Menu></Menu>
                ) : (<div></div>)
            }
        </>
    );
};

export default NavBar;