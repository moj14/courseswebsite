import React from 'react';
import {observer} from "mobx-react-lite";
import {NavBar, FootBar} from "../components/others/index";
import {Welcome, Courses, Video, ABitOfMaths, CoolLerning, Way, Graduate, Employment, Experience, Investing} from '../components/mainPage/index'

const MainPage = observer(() => {
    return (
        <>
            <NavBar></NavBar>
            <Welcome></Welcome>
            <Courses></Courses>
            <Video></Video>
            <ABitOfMaths></ABitOfMaths>
            <CoolLerning></CoolLerning>
            <Way></Way>
            <Graduate></Graduate>
            <Employment></Employment>
            <Experience></Experience>
            <Investing></Investing>
            <FootBar></FootBar>
        </>
    );
});

export default MainPage;